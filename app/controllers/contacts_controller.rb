class ContactsController < ApplicationController

  get '/contacts' do
    @page_title = 'Contacts'
    # TODO: scope this to @current_user
    @contacts = current_user.contacts.order(:name)
    haml :'contacts/index'
  end

  post '/contacts/import' do
    contacts = parse_vcard(params['file']['tempfile'])

    # TODO: insert this in bulk, make less horribly ridiculous
    contacts.each do |contact|
      if contact[:name] && contact[:email]
        next if Contact.where(email: contact[:email]).first
        c = Contact.where(contact).first_or_create
        c.groups << current_user.group
      end
    end

    #Contact.create(h)
    redirect '/contacts'
  end

  post '/contacts/send_sms' do
    send_sms(params[:body], params[:contact_ids])
    flash_success('SMS sent to selected users')
    redirect '/contacts'
  end

  post '/contacts/notify' do
    send_email(params[:subject], params[:body], params[:contact_ids])
    flash_success('Message sent to selected users')
    redirect '/contacts'
  end

  get '/contacts/delete/:id' do
    contact = current_user.contacts.where("contacts.id = ?", params[:id]).first

    if(contact)
      name = contact.name
      contact.destroy
      flash_success("Successfuly deleted contact: #{name}")
    else
      flash_fail("Could not delete contact")
    end

    redirect '/contacts'
  end

  private

  # very simple parser to extract name, email, phone number from vcard file (.vcf)
  def parse_vcard(vcard_file)
    i = 0
    entries = []

    File.readlines(vcard_file).each do |line|
      entries[i] ||= {}

      { /^FN:(.*)/       => :name,
        /EMAIL:(.*)/     => :email,
        /^TEL[^:]+:(.*)/ => :phone}.each do |regex, key|

        entries[i][key] = $1.strip if line =~ regex
      end

      i += 1 if(line['BEGIN:VCARD'])
    end

    entries
  end

end
