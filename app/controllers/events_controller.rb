require 'fileutils'

class EventsController < ApplicationController
  get '/events/:event_url' do
    @event = Event.where(event_url: params[:event_url]).first
    haml :'events/show'
  end

  get '/events' do
    @page_title = 'Events'
    @events = Event.order('start_time DESC').to_a
    haml :'events/index'
  end

  get '/event/new' do
    @page_title = 'New Event'
    @event = Event.new
    haml :'events/new'
  end

  get '/event/edit/:id' do
    id = params[:id]
    @page_title = 'Edit event'
    @event = Event.find(id)
    @form_action = "/event/update/#{id}"
    haml :'events/new'
  end

  # TODO: lol, clean up this hacktastic blob of event creation
  post '/event/create' do
    img = params.delete('banner_image')
    extension = File.extname(img['filename'])
    dest_dir = "public/img/banners/#{current_user.id}"
    dest_file = "#{dest_dir}/#{Time.now.to_i}#{extension}"
    params['banner_url'] = dest_file.sub(/^public/, '')

    FileUtils.mkdir_p(dest_dir)
    File.write(dest_file, File.read(img['tempfile']))
    Event.create(params)
    flash_success('Event created!')
    redirect '/events'
  end

  get '/event/:id/delete' do
    # flash_success('Event successfully deleted')
    # Event.destroy(params[:id])
    # redirect '/events'
  end

end
