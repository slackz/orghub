class Group < ActiveRecord::Base
  has_many :contacts_groups
  has_many :contacts, through: :contacts_groups

  has_many :events_groups
  has_many :events, through: :events_groups

  has_many :calendar_events_groups
  has_many :calendar_events, through: :calendar_events_groups
end
